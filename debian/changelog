kbackup (24.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (24.12.0).
  * Update build-deps and deps with the info from cmake.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 21 Dec 2024 23:23:35 +0100

kbackup (24.08.2-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (24.08.2).
  * Build against Qt6 / KF6, update build-deps and deps with the info from
    cmake.
  * Bump Standards-Version to 4.7.0, no change required.
  * Switch to declarative dh kf6 build sequence.
  * Update project homepage.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 14 Oct 2024 21:57:47 +0200

kbackup (23.08.2-1) unstable; urgency=medium

  * Team upload.

  [ Jesse Rhodes ]
  * New upstream release (23.08.2).
  * Update build-deps with info from cmake.

 -- Jesse Rhodes <jesse@sney.ca>  Sat, 21 Oct 2023 13:21:57 -0400

kbackup (22.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 01 Mar 2023 11:57:46 +0100

kbackup (22.12.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.12.1).
  * Bump Standards-Version to 4.6.2, no change required.
  * Add Albert Astals Cid’s master key to upstream signing keys.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 06 Jan 2023 22:34:07 +0100

kbackup (22.12.0-2) unstable; urgency=medium

  * Upload with a correctly released changelog.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 16:04:31 +0100

kbackup (22.12.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.08.3).
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.1, no change required.
  * New upstream release (22.11.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (22.12.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 10 Dec 2022 00:12:50 +0100

kbackup (21.12.3-1) unstable; urgency=medium

  [ Scott Kitterman ]
  * Remove myself from uploaders

  [ Aurélien COUDERC ]
  * New upstream release (21.12.3).
  * Bump Standards-Version to 4.6.0, no change required.
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 06 Mar 2022 21:25:38 +0100

kbackup (21.08.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.08.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 15:51:55 +0900

kbackup (21.04.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.04.0).
  * Added myself to the uploaders.
  * Borrow minimal upstream signing key from k3b.

 -- Norbert Preining <norbert@preining.info>  Wed, 28 Apr 2021 13:25:07 +0900

kbackup (20.12.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop the local man page, as upstream provides one already.

 -- Pino Toscano <pino@debian.org>  Sat, 09 Jan 2021 13:52:35 +0100

kbackup (20.12.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release (20.12.0).
  * Update build dependencies according to the upstream build system:
    - Qt >= 5.12
    - KF5 >= 5.71
  * Refresh copyright information.
  * Bump Standards-Version to 4.5.1, no change required.
  * Refresh upstream metadata.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 02 Jan 2021 12:01:43 +0100

kbackup (20.08.0-1) unstable; urgency=medium

  [ Scarlett Moore ]
  * New upstream release (20.08.0)
  * Bump Debhelper compat to 13.
  * Update upstream/metadata.
    - Add Bug-Database and Bug-Submit information.
    - Update repository url to invent.kde.org.
    - Fix the ordering of fields.
    - Remove obsolete name/contact fields.
  * Update Homepage field in debian/control.
  * Cleanup format/files:
    - changelog cleanup.
      - Group and condense entries into easier to read
        format.
    - (prior to knowing it should be left in, due
        to git history issues)
      - gbp removal cleanup:
       - Extra line, adjust for gbp cleanup.
       - Remove upstream remaining cruft.
       - Remove gbp.conf as it should be local.
    - wrap-and-sort
  * Update manpage kbackup.1.
    - Update to current help options.
    - Converted to docbook for upstreaming.
  * Update copyright file.
    - Update to current copyright:
      - Diff versions 19.12.3, 20.04.0, 20.04.3, 20.08.0.
    - Add new field Upstream-Contact.
  * Add override for maintainer-manual-page as
    it is being upstreamed.
  * Use hardening=+all in rules to utilize all hardening options.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Sun, 23 Aug 2020 02:37:07 -0700

kbackup (19.12.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards version to 4.5.0; No changes required.
  * Update salsa CI config file with team common variables.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Thu, 19 Mar 2020 18:06:26 -0700

kbackup (19.12.1-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 11 Jan 2020 03:01:52 -0500

kbackup (19.12.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file to the new release-service location.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.5
    - bump Qt to 5.11.0
    - bump/align KF packages to 5.45.0
    - explicitly add libkf5archive-dev, libkf5iconthemes-dev, and
      libkf5widgetsaddons-dev
  * Switch from the debhelper build dependency to debhelper-compat
    - remove debian/compat
  * Bump Standards-Version to 4.4.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Mon, 06 Jan 2020 06:52:20 +0100

kbackup (19.08.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards to 4.4.0.1;
    - Add new Rules-Requires-Root to control file.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Thu, 26 Sep 2019 06:13:48 -0700

kbackup (19.08.0-1) unstable; urgency=medium

  * Fix "kbackup's docbook handbook files cannot be read by khelpcentre"
    - Add with=kf5 to rules file. (Closes: #934808)
  * New upstream release.
  * Bump standards to 4.4.0; no changes needed.
  * Update copyright.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Thu, 22 Aug 2019 06:22:54 -0700

kbackup (19.04.3-1) unstable; urgency=medium

  * New upstream release.
  * Refresh copyright file.
  * Bump compat to 12.
  * Bump standards to 4.3.0 - No changes needed.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Wed, 17 Jul 2019 18:12:29 -0700

kbackup (18.12.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Uploaders to my new name.
  * Update Copyright to my new name.
  * Update Standards version to 4.2.1 without further changes.
  * Update signing key to minimal as per lintian error.
  * Update copyright file.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Sun, 03 Feb 2019 11:54:40 -0700

kbackup (18.04.0-1) unstable; urgency=low

  * New upstream release. (Closes: #896695)
  * Update maintainer to KDE team and add myself and Scott Kitterman
    to uploaders.
  * Update compat to 11.
    - Bump debhelper (>= 11)
  * Update homepage.
  * Add VCS entries.
  * Update watch file.
    - Update to version 4.
    - Add pgp support with upstream key.
    - Update url.
  * Update copyright file to format 1.0
  * Update build dependencies for qt5 port. wrap-and-sort.
  * Update standards version to 4.1.4 without further changes.
  * Add hardening+=bindnow.
  * Update upstream metadata
  * Remove --with kde from rules.
  * Update copyright.
    - Add LGPL copyright files to copyright.
    - Add po copyright authors.
    - Fix indentation
    - Remove extra po/* entry.

 -- Scarlett Clark <sgclark@kde.org>  Tue, 08 May 2018 07:59:16 -0700

kbackup (0.8-2) unstable; urgency=low

  * Upload to unstable
  * Update standards version to 3.9.4 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 29 Apr 2013 18:39:07 -0400

kbackup (0.8-1) experimental; urgency=low

  * New upstream release
    - Update debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Mon, 23 Jul 2012 08:51:28 -0400

kbackup (0.7.1-3) unstable; urgency=low

  * Drop mistaken override_dh_auto_install from last upload

 -- Scott Kitterman <scott@kitterman.com>  Mon, 25 Jun 2012 10:19:58 -0400

kbackup (0.7.1-2) unstable; urgency=low

  * Bump debian/compat and minimum debhelper build-depend to 9 for hardening
    flags
  * Bump standards version to 3.9.3 without further change
  * Add override in debian/rules to manually remove mysterious dangling
    symlinks from HTML documentation

 -- Scott Kitterman <scott@kitterman.com>  Mon, 25 Jun 2012 01:06:49 -0400

kbackup (0.7.1-1) unstable; urgency=low

  * New upstream release
  * Updated watch file for new download location (thanks to Felix Geyer for
    the update)
  * Update copyright year in debian/copyright

 -- Scott Kitterman <scott@kitterman.com>  Sat, 05 Mar 2011 09:47:07 -0500

kbackup (0.7-2) unstable; urgency=low

  * Add README.Debian to document Debian specific requirements for running
    as root (Closes: #596903)
  * Update debian/watch so it works
  * Update standards version to 3.9.1 without further change

 -- Scott Kitterman <scott@kitterman.com>  Mon, 31 Jan 2011 15:51:32 -0500

kbackup (0.7-1) unstable; urgency=low

  * Initial release (Closes: #588926)

 -- Scott Kitterman <scott@kitterman.com>  Tue, 13 Jul 2010 15:10:15 -0400
